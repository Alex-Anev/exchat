defmodule Client do

  def send_msg(clnt_pid, msg) do
    send(clnt_pid, {:send, msg})
  end

  def user({srv_pid, name}) do

    receive do
      {:send, msg} ->
        send(srv_pid, {:create, {name, msg}})
      :read_messages ->
        send(srv_pid, {:read, self()})
      {:receive_messages, messages} ->
        IO.inspect messages
    end

    user({srv_pid, name})
    
  end

end