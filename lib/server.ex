defmodule Server do

  def start_srv do
    spawn(fn -> Server.main_room([]) end)
    |> Process.register(:srv)
  end

  def start_client(name) do
    send(:srv, {:new_client, name})
  end

  def main_room(messages) do

    new_messages = receive do
      {:new_client, name} ->
        spawn(fn -> Client.user({self(), name}) end)
        |> Process.register(String.to_atom(name))
        messages
      {:create, msg} ->
        [messages | msg]
      {:read, pid} ->
        send(pid, messages)
        messages
    end

    main_room(new_messages)
    
  end

end
